[![Clippy](https://github.com/nogibjj/rust-data-engineering/actions/workflows/lint.yml/badge.svg)](https://github.com/nogibjj/rust-data-engineering/actions/workflows/lint.yml)
[![Tests](https://github.com/nogibjj/rust-data-engineering/actions/workflows/tests.yml/badge.svg)](https://github.com/nogibjj/rust-data-engineering/actions/workflows/tests.yml)


# Individual Project1

## Project Description

This project is about creating a static site with [zola](https://www.getzola.org) and deploying it to [Gitlab](https://gitlab.com/yy379/week1-mini-project). This site will hold all of the portofolio work in IDS721 Cloud Computing class.

## Deploy the Site

1. Install zola (Details in the [Install Zola](https://www.getzola.org/documentation/getting-started/installation/) section)
```bash
$ brew install zola
```
2. Clone the repository
```bash
$ git clone
```
3. Change the directory
```bash
$ cd anemone
```
4. Run the site locally
```bash
$ zola serve
```
5. Test the site locally
```bash
$  http://127.0.0.1:1111
```

6. Deploy the site using vercel
![deploy](Deploy.png)
## Demo
Link to the video demo: [Demo](https://drive.google.com/file/d/1T-0ap8Hm43VbGjchHZD_OqydsfOPGVMK/view?usp=sharing)

![homepage](home.png)
![blog](blog.png)

## References

* [Getting Started with Zola](https://www.getzola.org/documentation/getting-started/overview/)
* [GitHub Copilot CLI](https://www.npmjs.com/package/@githubnext/github-copilot-cli)
* [Rust Fundamentals](https://github.com/alfredodeza/rust-fundamentals)
* [Rust Tutorial](https://nogibjj.github.io/rust-tutorial/)
* [Rust MLOps Template](https://github.com/nogibjj/mlops-template)
